# bkp
Duplicity Backup Docker Image
### Supported 2 modes:

#### Mode Cron
In this mode container is always in running state and execute jobs defined in /etc/cron.d/cron file

#### Mode Kubernetes Cron
In this mode cron schedule executes by Kubernetes ( kind: CronJob ) 
By default it execute /usr/local/bin/bkp-script.sh file. Use configmap to overwrite it