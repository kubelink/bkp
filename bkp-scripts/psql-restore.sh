#!/bin/bash
set -e
# Any subsequent(*) commands which fail will cause the shell script to exit immediately
dbname=$1
time=$2
save_path=/opt/backups
archive_dir=/opt/bkp-cache

# Print Variables
printf "Current date and time %s\n" "$now \n"
printf "Cloud Provider is: ${CLOUD_PROVIDER} \n"
printf "Docker image version: ${CI_COMMIT_SHORT_SHA} \n"
printf "Restore database is: $dbname \n"
printf "Source: ${MEDIA} \n"

## Check if source media (bucket) is set
if [ -z "${MEDIA}" ]; then
    printf "Please set environment variable MEDIA \n"
    exit 1
fi

if [[ -z "$time" ]]; then
    iftimeset=""
else
    iftimeset="--time $time"
fi

if [[ ( -z "$dbname" ) ]]; then
    echo "Must provide database and env, example: postgres-restore.sh mydb env04" 1>&2
    exit 1
fi


  mkdir -p $save_path
  rm -rf $save_path/*

echo -e "\n#### Download"
duplicity \
--file-to-restore $save_path/$dbname.sql \
$iftimeset \
${MEDIA} \
$save_path/$dbname.sql \
--no-encryption \
--verbosity info \
--archive-dir /opt/bkp-cache \
--force

echo -e "\n#### Set permissions for folder"
cd $save_path

echo -e "\n####List files to restore"
ls -la 

echo -e "\n#### Starting db restore"
echo -e "\n#### Disconnect active sessions"

psql -d postgres -h $DB_HOST -U postgres -w -c "
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = '$dbname'
  AND pid <> pg_backend_pid();"

echo -e "\n####Drop DB"
set +e
psql -h $DB_HOST -U postgres -w -d postgres -c "DROP DATABASE $dbname"
set -e

echo -e "\n####Create a new empty DB"
psql -h $DB_HOST -U postgres -w -d postgres -c "CREATE DATABASE $dbname"

echo -e "\n#### Starting db restore"
psql -h $DB_HOST -U postgres -w -f $save_path/$dbname.sql

echo -e "\n#### Changing postgres db owner"
psql -h $DB_HOST -U postgres -w -c "ALTER DATABASE $dbname OWNER TO postgres"
