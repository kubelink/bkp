#!/bin/bash
now="$(date)"
archive_dir=/opt/bkp-cache
set -e

## METHODS
function hr() {
  printf '=%.0s' {1..100}
  printf "\n"
}

## Set cloud provider specific backup arguments
hr
if [ ${CLOUD_PROVIDER} = "azure" ]; then
  CLOUD_SPECIFIC_ARG="--azure-max-connections 1"

elif [ ${CLOUD_PROVIDER} = "aws" ]; then
  CLOUD_SPECIFIC_ARG="--s3-multipart-chunk-size 100 --s3-multipart-max-procs 1 --s3-european-buckets --s3-use-ia --s3-use-server-side-encryption --s3-use-new-style --asynchronous-upload"

elif [ ${CLOUD_PROVIDER} = "scw" ]; then
  CLOUD_SPECIFIC_ARG="--s3-multipart-chunk-size 100 --s3-multipart-max-procs 1 --asynchronous-upload"

else
  printf "failure, please set variable CLOUD_PROVIDER"
  exit 1
fi

hr

# Print Variables
printf "Current date and time %s\n" "$now \n"
printf "Cloud Provider is: ${CLOUD_PROVIDER} \n"
printf "Backup source is: ${FS_BKP_SRC} \n"
printf "Backup destination is: ${MEDIA} \n"
printf "Docker image version: ${CI_COMMIT_SHORT_SHA} \n"

# Run Backup

mkdir -p $archive_dir
hr
printf "############## Backup selected folders ###################\n"

duplicity \
  --exclude ${FS_BKP_SRC}/var \
  --exclude ${FS_BKP_SRC}/.github \
  --exclude ${FS_BKP_SRC}/lost+found \
  --exclude ${FS_BKP_SRC}/wp-content/cache \
  --include ${FS_BKP_SRC} \
  --exclude '**' \
  --allow-source-mismatch \
  --full-if-older-than 6M \
  --volsize 4000 \
  --no-encryption \
  --no-compression \
  --archive-dir $archive_dir \
  $CLOUD_SPECIFIC_ARG \
  / ${MEDIA}

hr
printf "############ Backup Archive Maintenance #########\n"

duplicity remove-all-inc-of-but-n-full 4 --force --archive-dir $archive_dir $MEDIA
duplicity remove-all-but-n-full 16 --force --archive-dir $archive_dir $MEDIA
