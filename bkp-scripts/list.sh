#!/bin/bash
time=$1
CUSOM_MEDIA_URI=$2
now="$(date)"
s3=$S3_BUCKET_URL
archive_dir=/opt/bkp-cache

## METHODS
function hr(){
  printf '=%.0s' {1..100}
  printf "\n"
}

printf "Docker image version: ${CI_COMMIT_SHORT_SHA} \n"
printf "Current date and time %s\n" "$now"

mkdir -p $archive_dir
hr
## Check if custom backup media specified
if [ -z "$CUSOM_MEDIA_URI" ]; then
    MEDIA=${MEDIA}
else
    MEDIA=$CUSOM_MEDIA_URI
fi

## Check if time specified
if [ -z "$time" ]; then
    time="now"
fi

## Print Variables
printf "Current date and time %s\n" "$now \n"
printf "Cloud Provider is: ${CLOUD_PROVIDER} \n"
printf "List files from backup back in time: $time \n"
printf "Backup media you working with is: ${MEDIA} \n"
hr

cd /tmp
#### list backup files on S3 destination
printf "Start Duplicity \n"
duplicity \
list-current-files \
--time=$time \
--archive-dir $archive_dir \
$MEDIA \
--verbosity info