#!/bin/bash

## Usage: fs-restore.sh [2M] [ webroot/vendor/zendframework/zend-view/src ] [custom-destination]
## if no folder defined then full restore will be performed to original destination

time=$1
file=$2
dest=$3
archive_dir=/opt/bkp-cache

## METHODS
function hr(){
  printf '=%.0s' {1..100}
  printf "\n"
}
if [ -z "${MEDIA}" ]; then
    printf "Please set environment variable MEDIA \n"
    exit 1
fi


if [ -z "$file" ]; then
    file_to_restore="/"
    destination="/"
else
    file_to_restore=$file
    destination="/$file"
fi

if [ -z "$dest" ]; then
    echo "\nNo destination not defined, restoring to original location"
else
    destination="$dest"
fi
if [ -z "$time" ]; then
    iftimeset=""
else
    iftimeset="--time $time"
fi


hr
# Print Variables
printf "Current date and time %s\n" "$now \n"
printf "Docker image version: ${CI_COMMIT_SHORT_SHA} \n"
printf "Restore to: $dest \n"
printf "Restore from media: ${MEDIA} \n"
hr
mkdir -p $archive_dir

duplicity \
--file-to-restore $file_to_restore \
--force \
$iftimeset \
--ignore-errors \
--archive-dir $archive_dir \
--no-encryption \
${MEDIA} \
$destination \
--verbosity info
