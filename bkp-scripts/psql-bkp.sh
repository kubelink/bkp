#!/bin/bash
set -e
now="$(date)"
DB_HOST=${DB_HOST}
save_path=/opt/backups
archive_dir=/opt/bkp-cache

## METHODS

function hr(){
  printf '=%.0s' {1..100}
  printf "\n"
}

function echo_status(){
  printf '\r'; 
  printf ' %0.s' {0..100} 
  printf '\r'; 
  printf "$1"'\r'
}

function backup_databases(){
  mkdir -p $save_path
  rm -rf $save_path/*
  #### Getting db list
  FULL_BACKUP_QUERY="select datname from pg_database where not datistemplate and datallowconn order by datname;"

  #### Database dump
  echo -e "\nStarting Databases dump"
  for DATABASE in `psql -h $DB_HOST -U postgres -w -At -c "$FULL_BACKUP_QUERY" postgres`
      do
          if ! pg_dump -h $DB_HOST -U postgres -w -Fp -c -E UTF-8 -b --no-owner --no-acl -C "$DATABASE" -f $save_path/$DATABASE.sql; then
                  echo "[!!ERROR!!] Failed to produce dump database $DATABASE" 1>&2
          fi
  done

}


function upload(){
    duplicity \
    --include $save_path \
    --exclude '**' \
    --allow-source-mismatch \
    --full-if-older-than 6M \
    --volsize 4000 \
    --no-encryption \
    --no-compression \
    --archive-dir $archive_dir \
    $CLOUD_SPECIFIC_ARG \
    / $MEDIA
}

#### Maintenance job for deleting old incremental files
function maintenace(){
    echo -e "\nMaintenance job for deleting old incremental files"
    duplicity remove-all-inc-of-but-n-full 4 --archive-dir /opt/bkp-cache --force $MEDIA
    duplicity remove-all-but-n-full 16 --archive-dir /opt/bkp-cache --force  $MEDIA
}

## Check if source media (bucket) is set
if [ -z "${MEDIA}" ]; then
    printf "Please set environment variable MEDIA \n"
    exit 1
fi

## Check if source database server hostname is set
if [ -z "${DB_HOST}" ]; then
    printf "Please set environment variable DB_HOST \n"
    exit 1
else
DB_HOST=${DB_HOST}
fi

## Set cloud provider specific backup arguments
hr
if [ ${CLOUD_PROVIDER} = "azure" ]; 
    then CLOUD_SPECIFIC_ARG="--azure-max-connections 1"

elif [ ${CLOUD_PROVIDER} = "aws" ] 
     then CLOUD_SPECIFIC_ARG="--s3-multipart-chunk-size 100 --s3-multipart-max-procs 1 --s3-european-buckets --s3-use-ia --s3-use-server-side-encryption --s3-use-new-style --asynchronous-upload"

elif [ ${CLOUD_PROVIDER} = "scw" ] 
     then CLOUD_SPECIFIC_ARG="--s3-multipart-chunk-size 100 --s3-multipart-max-procs 1 --asynchronous-upload"


else
    printf  "failure, please set variable CLOUD_PROVIDER"
    exit 1
fi

# Print Variables
printf "Docker image version: ${CI_COMMIT_SHORT_SHA} \n"
printf "Current date and time %s\n" "$now \n"
printf "Cloud Provider is: ${CLOUD_PROVIDER} \n"
printf "Backup source is: $save_path \n"
printf "Backup destination is: ${MEDIA} \n"


# RUN SCRIPT
hr
backup_databases
hr
echo -e "\n####List files to upload"
ls -lah $save_path
hr
upload
hr
maintenace
