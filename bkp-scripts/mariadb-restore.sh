#!/bin/bash
set -e
# Any subsequent(*) commands which fail will cause the shell script to exit immediately
dbname=$1
time=$2
now="$(date)"
save_path=/opt/backups
archive_dir=/opt/bkp-cache

# Print Variables
printf "Current date and time %s\n" "$now \n"
printf "Cloud Provider is: ${CLOUD_PROVIDER} \n"
printf "Docker image version: ${CI_COMMIT_SHORT_SHA} \n"
printf "Restore database is: $dbname \n"
printf "Source: ${MEDIA} \n"

## Check if source media (bucket) is set
if [ -z "${MEDIA}" ]; then
    printf "Please set environment variable MEDIA \n"
    exit 1
fi

if [[ -z "$time" ]]; then
    iftimeset=""
else
    iftimeset="--time $time"
fi

  mkdir -p $save_path
  rm -rf $save_path/*


# Linux bin paths
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"

# Run duplicity restore
echo -e "\n#### Download"

duplicity \
--file-to-restore $save_path/$dbname.sql \
$iftimeset \
${MEDIA} \
$save_path/$dbname.sql \
--no-encryption \
--verbosity info \
--archive-dir /opt/bkp-cache \
--force

echo -e "\n#### Set permissions for folder"
cd $save_path

echo -e "\n####List files to restore"
ls -la 

echo -e "\n#### Starting db restore"
mysql -h $DB_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $dbname < $save_path/$dbname.sql
