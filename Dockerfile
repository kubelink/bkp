FROM rockylinux:8

# Environment
ENV PS1 "\n\n>> bkp \W \$ "
ENV PATH $APP_ROOT/bin:$PATH
ENV TERM=xterm
ENV CLOUD_PROVIDER=scw

# Package Repositories
RUN dnf install -y \
            dnf-utils \
            epel-release \
            https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm \
    && dnf -y --setopt=tsflags=nodocs update \
    # Disable the built-in PostgreSQL module:
    && dnf -qy module disable postgresql \
    && dnf -y clean all


# Packages
ENV PACKAGES \
    duplicity \
    wget \
    which \
    mariadb \
    python3-pip \
    postgresql14

RUN dnf -y --setopt=tsflags=nodocs install $PACKAGES \
    && dnf clean all

# Update pip3
RUN pip3 install --upgrade setuptools pip

# Install some Python packages
RUN pip3 install -U wheel boto boto3 requests setuptools_rust azure-storage-blob

# Prepare folder for Duplicity cache
RUN mkdir -p /opt/bkp-cache /opt/backups  \
    && chown -R 1001:root /opt \
    && chmod -R 775 /opt

## Backup Container Entrypoint and backup scripts
COPY bkp-scripts/ /usr/local/bin/

## Set permissions for application
RUN chown -R 1001:root /var/log /run  /usr/local/bin/*
RUN chmod -R g=u /etc/passwd /var/log /run 
RUN chmod -R +x   /usr/local/bin/*

# Ready
USER 1001
CMD ["/usr/local/bin/fs-bkp.sh"]

ARG CI_COMMIT_SHORT_SHA
ENV CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}
RUN echo "Docker image version: $CI_COMMIT_SHORT_SHA"